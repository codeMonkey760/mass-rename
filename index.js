const fs = require('fs');
const prompt = require('prompt-async');
const path = require('path');

function arrayHasAnyItems(array, items) {
    return (items.map((element) => array.includes(element))).includes(true);
}

function processCommandLineOptions(args) {
    const config = {
        help: false,
        interactive: false,
        dryRun: false,
    };

    if (
        arrayHasAnyItems(args, ['/?', '-h', '--help'])
        || args.length < 4
    ) {
        return {help: true};
    }

    if (arrayHasAnyItems(args, ['-i', '--interactive'])) {
        config.interactive = true;
    }

    if (arrayHasAnyItems(args, ['-d', '--dry-run'])) {
        config.dryRun = true;
    }

    config.fromRegex = new RegExp(args[2]);
    config.toPattern = args[3];

    return config;
}

async function main() {
    const config = processCommandLineOptions(process.argv);

    if (config.help) {
        console.log("USAGE STRING");

        process.exit(1);
    }

    const fileList = fs.readdirSync('.').filter((fileName) => config.fromRegex.test(fileName));
    const renameMapping = {};

    if (fileList.length < 1) {
        console.log('File name regex failed to capture files. Please check regex.');

        process.exit(1);
    }

    fileList.forEach((orgFileName) => {
        renameMapping[orgFileName] = orgFileName.replace(config.fromRegex, config.toPattern);
    });

    for (const orgFileName in renameMapping) {
        if (config.dryRun) {
            console.log(orgFileName + " => " + renameMapping[orgFileName]);
        } else if (config.interactive) {
            const msg = `Rename '${orgFileName}' to '${renameMapping[orgFileName]}'? (y/n)`;
            const result = await prompt.confirm([msg]);
            if (result) {
                fs.renameSync(path.join('.', orgFileName), path.join('.', renameMapping[orgFileName]));
            }
        } else {
            fs.renameSync(path.join('.', orgFileName), path.join('.', renameMapping[orgFileName]));
        }
    }
}

main().then();